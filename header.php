<?php
   /**
    * The header for our theme
    *
    * This is the template that displays all of the <head> section and everything up until <div id="content">
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package Juan_Bautista
    */
   
   ?>
<!doctype html>
<html <?php language_attributes(); ?>>
   <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="profile" href="https://gmpg.org/xfn/11">
      <?php wp_head(); ?>
   </head>
   <body <?php body_class();?>>
      <div id="page" class="site">
      <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'juan-bautista' ); ?></a>
      <header id="masthead" class="navbar">
         <div id="myNav" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <div class="overlay-content">
               <?php
                  wp_nav_menu( array(
                     'theme_location' => 'menu-1',
                     'menu_id'        => 'primary-menu',
                  ) );
                  ?>
            </div>
         </div>
         <div style="display: scroll;position:fixed;top:10px;right:10px; z-index: 100;">
            <span style="font-size:20px;cursor:pointer" onclick="openNav()">&#9776;menu</span>
            <br>     
         </div>
      </header>
      <br>
      <div class="divider text-center" data-content="<?php bloginfo( 'name' ); ?>"></div>
      <!-- #masthead -->
      <div id="content" class="site-content">