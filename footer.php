<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Juan_Bautista
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="section section-footer">
		<div class="docs-footer container grid-lg site-info" id="copyright">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'juan-bautista' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'juan-bautista' ), 'WordPress' );
				?>
			</a>
			<span class="sep"></span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'juan-bautista' ), 'juan-bautista');
				?>
				<p>Desarrollador con  <span class="text-error">♥</span> por <a href="./" target="_blank">Juan Bautista</a></p>
		</div><!-- .site-info -->

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}
</script>

</body>
</html>
