=== Juan Bautista ===

Colaboradores: automattic
Etiquetas: fondo personalizado, logotipo personalizado, menú personalizado, imágenes destacadas, comentarios enlazados, listas para la traducción

Requiere al menos: 4.5
Probado hasta: 4.8
Etiqueta estable: 1.0.0
Licencia: GNU General Public License v2 o posterior
Licencia URI: LICENCIA

Un tema de inicio llamado Juan Bautista.

== Descripción ==

Descripción

== Instalación ==

1. En su panel de administración, vaya a Apariencia> Temas y haga clic en el botón Agregar nuevo.
2. Haga clic en Cargar tema y Elegir archivo, luego seleccione el archivo .zip del tema. Haga clic en Instalar ahora.
3. Haga clic en Activar para usar su nuevo tema de inmediato.

== Preguntas frecuentes ==

= ¿Este tema soporta algún plugin? =

Juan Bautista incluye soporte para Infinite Scroll en Jetpack.